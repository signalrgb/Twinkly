# Twinkly

[![Add To Installation](https://marketplace.signalrgb.com/resources/add-extension-256.png 'Add to My SignalRGB Installation')](signalrgb://addon/install?url=https://gitlab.com/signalrgb/Twinkly)

## Getting started
This is a simple SignalRGB Addon to add support for Twinkly Wifi Devices to SignalRGB.

## Known Issues
- No Support For First Generation Devices (Need Users with First Gen Devices)
- Second Gen Devices With Older Firmware Version May Require a Firmware Update.

## Installation
Click the button above and allow signalrgb to install this extension when prompted.

## Support
Feel free to open issues here, or join the SignalRGB Testing Server and post an issue there https://discord.com/invite/J5dwtcNhqC.